package com.dolnikova.tm.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Project {

    private UUID id;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private List<Task> tasks;

    public Project(String projectName) {
        this.name = projectName;
        id = UUID.randomUUID();
        tasks = new ArrayList<>();
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public Task readTask(int taskNumber) {
        return tasks.get(taskNumber - 1);
    }

    public void updateTask(int taskNumber, Task newTask) {
        tasks.remove(taskNumber - 1);
        tasks.add(taskNumber - 1, newTask);
    }

    public void deleteTask(int taskNumber) {
        tasks.remove(taskNumber - 1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
