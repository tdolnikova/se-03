package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.Message;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.TaskRepository;

import java.util.Scanner;

public class Bootstrap {

    private static final String HELP_STRING = Message.HELP + Message.COLON + Message.SHOW_ALL_COMMANDS + "\n"
            + Message.CREATE_PROJECT + Message.COLON + Message.CREATE_PROJECT + "\n"
            + Message.READ_PROJECT + Message.COLON + Message.READ_PROJECT + "\n"
            + Message.UPDATE_PROJECT + Message.COLON + Message.UPDATE_PROJECT + "\n"
            + Message.DELETE_PROJECT + Message.COLON + Message.DELETE_PROJECT + "\n"
            + Message.CREATE_TASK + Message.COLON + Message.DELETE_TASK + "\n"
            + Message.READ_TASK + Message.COLON + Message.READ_TASK + "\n"
            + Message.UPDATE_TASK + Message.COLON + Message.UPDATE_TASK + "\n"
            + Message.DELETE_TASK + Message.COLON + Message.DELETE_TASK + "\n"
            + Message.PRESS_ENTER + Message.COLON + Message.EXIT;

    public void init() {
        System.out.println(Message.WELCOME);
        Scanner scanner = new Scanner(System.in);
        ProjectRepository projectManager = new ProjectRepository();
        TaskRepository taskManager = new TaskRepository();
        String input = "exit";
        while (!input.isEmpty()) {
            input = scanner.nextLine();
            switch (input) {
                case Message.HELP:
                    showHelp();
                    break;
                case Message.CREATE_PROJECT:
                    projectManager.createProject();
                    break;
                case Message.READ_PROJECT:
                    projectManager.readProject();
                    break;
                case Message.UPDATE_PROJECT:
                    projectManager.updateProject();
                    break;
                case Message.DELETE_PROJECT:
                    projectManager.deleteProject();
                    break;
                case Message.CREATE_TASK:
                    taskManager.createTask();
                    break;
                case Message.READ_TASK:
                    taskManager.readTask();
                    break;
                case Message.UPDATE_TASK:
                    taskManager.updateTask();
                    break;
                case Message.DELETE_TASK:
                    taskManager.deleteTask();
                    break;
                default:
                    if (!input.isEmpty())
                        System.out.println(Message.COMMAND_DOESNT_EXIST);
                    break;
            }
        }
    }

    private static void showHelp() {
        System.out.println(HELP_STRING);
    }

}
