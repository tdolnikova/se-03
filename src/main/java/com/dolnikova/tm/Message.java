package com.dolnikova.tm;

public class Message {

    /*
     * General messages
     * */

    public final static String WELCOME = "* * * Welcome to Task Manager * * *";
    public final static String COMMAND_DOESNT_EXIST = "Такой команды не существует";
    public final static String HASH = "#";
    public final static String COLON = ": ";
    public final static String INCORRECT_NUMBER = "Некорректный ввод числа.";
    public final static String TRY_AGAIN = "Попробуйте еще раз.";

    /*
     * Help messages
     * */

    public final static String HELP = "help";
    public final static String SHOW_ALL_COMMANDS = "show all commands";
    public final static String CREATE_PROJECT = "create project";
    public final static String READ_PROJECT = "read project";
    public final static String UPDATE_PROJECT = "update project";
    public final static String DELETE_PROJECT = "delete project";
    public final static String CREATE_TASK = "create task";
    public final static String READ_TASK = "read task";
    public final static String UPDATE_TASK = "update task";
    public final static String DELETE_TASK = "delete task";
    public final static String PRESS_ENTER = ">> press enter <<";
    public final static String EXIT = "exit";

    /*
    * Project messages
    * */

    public final static String CHOOSE_PROJECT = "Выберите проект";
    public final static String IN_PROJECT = "В проекте ";
    public final static String PROJECT = "Проект";
    public final static String CREATED_M = "создан.";
    public final static String NO_PROJECTS = "Проектов нет.";
    public final static String PROJECT_UPDATED = "Проект обновлен.";
    public final static String PROJECT_DELETED = "Проект удален.";
    public final static String INSERT_PROJECT_NAME = "Введите название проекта.";
    public final static String PROJECT_NAME_DOESNT_EXIST = "Проекта с таким названием не существует.";
    public final static String WHICH_PROJECT_DELETE = "Какой проект хотите удалить?";
    public final static String NO_TASKS_IN_PROJECT = "В выбранном проекте нет задач";

    /*
     * Task messages
     * */

    public final static String TASK = "Задача";
    public final static String CREATED_F = "создана.";
    public final static String INSERT_TASK = "Введите задачу: ";
    public final static String INSERT_NEW_TASK = "Введите новую задачу";
    public final static String INSERT_TASK_NUMBER = "Введите номер задачи.";
    public final static String INSERT_NEW_PROJECT_NAME = "Введите название нового проекта.";
    public final static String TASK_UPDATED = "Задача изменена.";
    public final static String TASK_DELETED = "Задача удалена.";
    public final static String OF_TASKS_WITH_POINT = "задач.";

}
