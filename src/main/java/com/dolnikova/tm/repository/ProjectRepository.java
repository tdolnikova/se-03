package com.dolnikova.tm.repository;

import com.dolnikova.tm.Message;
import com.dolnikova.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectRepository {

    private static Scanner scanner = new Scanner(System.in);
    public static List<Project> projects = new ArrayList<>();

    public void createProject() {
        System.out.println(Message.INSERT_NEW_PROJECT_NAME);
        String projectName = scanner.nextLine();
        if (projectName.isEmpty()) return;
        for (Project project : projects) {
            if (projectName.equals(project.getName())) {
                System.out.println(Message.PROJECT_NAME_DOESNT_EXIST);
                return;
            }
        }
        projects.add(new Project(projectName));
        System.out.println(Message.PROJECT + " " + projectName + " " + Message.CREATED_M);
    }

    public void readProject() {
        if (projects.isEmpty()) {
            System.out.println(Message.NO_PROJECTS);
            return;
        }
        System.out.println(Message.INSERT_PROJECT_NAME);
        boolean fileFound = false;
        boolean tasksFound = false;
        boolean projectRead = false;
        while (!projectRead) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) break;
            for (Project project : projects) {
                if (projectName.equals(project.getName())) {
                    fileFound = true;
                    if (project.getTasks().size() != 0) tasksFound = true;
                    for (int i = 0; i < project.getTasks().size(); i++) {
                        System.out.println(Message.HASH.toString() + (i + 1) + " " + project.getTasks().get(i).getName());
                    }
                    projectRead = true;
                }
            }

            if (!fileFound) System.out.println(Message.PROJECT_NAME_DOESNT_EXIST + " " + Message.TRY_AGAIN);
            else if (!tasksFound) {
                System.out.println(Message.NO_TASKS_IN_PROJECT);
                projectRead = true;
            } else projectRead = true;
        }
    }

    public void updateProject() {
        System.out.println(Message.INSERT_PROJECT_NAME);
        boolean fileFound = false;
        while (!fileFound) {
            String projectName = scanner.nextLine();
            for (Project project : projects) {
                if (projectName.equals(project.getName())) {
                    fileFound = true;
                    System.out.println(Message.INSERT_NEW_PROJECT_NAME);
                    while (true) {
                        String newProjectName = scanner.nextLine();
                        if (!newProjectName.isEmpty()) {
                            project.setName(newProjectName);
                            System.out.println(Message.PROJECT_UPDATED);
                            break;
                        }
                    }
                }
            }
            if (!fileFound) System.out.println(Message.PROJECT_NAME_DOESNT_EXIST.toString() + Message.TRY_AGAIN.toString());
        }
    }

    public void deleteProject() {
        if (projects.isEmpty()) {
            System.out.println(Message.NO_PROJECTS);
            return;
        }
        System.out.println(Message.WHICH_PROJECT_DELETE);
        Project project = selectProject();
        if (project != null) {
            projects.remove(project);
            System.out.println(Message.PROJECT_DELETED);
        }
    }

    private static Project selectProject() {
        String projectName;
        while (true) {
            projectName = scanner.nextLine();
            if (projectName.isEmpty()) return null;
            for (Project project : projects) {
                if (projectName.equals(project.getName())) return project;
            }
            System.out.println(Message.PROJECT_NAME_DOESNT_EXIST.toString() + Message.TRY_AGAIN.toString());
        }
    }

}
