package com.dolnikova.tm.repository;

import com.dolnikova.tm.Message;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

import java.util.Scanner;

import static com.dolnikova.tm.repository.ProjectRepository.projects;

public class TaskRepository {

    private static Scanner scanner = new Scanner(System.in);

    public void createTask() {
        System.out.println(Message.CHOOSE_PROJECT);
        boolean taskCreated = false;
        boolean fileFound = false;
        while (!taskCreated) {
            String projectName = scanner.nextLine();
            for (Project project : projects) {
                if (projectName.equals(project.getName())) {
                    fileFound = true;
                    System.out.println(Message.INSERT_TASK);
                    while(!taskCreated) {
                        String taskText = scanner.nextLine();
                        if (taskText.isEmpty()) taskCreated = true;
                        else {
                            project.addTask(new Task(project.getId(), taskText));
                            System.out.println(Message.TASK + " " + taskText + " " + Message.CREATED_F);
                        }
                    }
                }
            }
            if (!fileFound) {
                System.out.println(Message.PROJECT_NAME_DOESNT_EXIST);
                taskCreated = true;
            }
        }
    }

    public void readTask() {
        System.out.println(Message.INSERT_PROJECT_NAME);
        Project project = selectProject();
        if (project != null) {
            System.out.println(Message.IN_PROJECT + project.getName() + " " + project.getTasks().size() + " " + Message.OF_TASKS_WITH_POINT + " " + Message.INSERT_TASK_NUMBER);
            boolean taskRead = false;
            while (!taskRead) {
                String taskNumber = scanner.nextLine();
                if (taskNumber.isEmpty()) break;
                if (isNumber(taskNumber)) {
                    int num = Integer.parseInt(taskNumber);
                    if (taskExists(num, project.getTasks().size())) {
                        System.out.println(Message.HASH.toString() + num + " " + project.readTask(num).getName());
                        taskRead = true;
                    }
                    else System.out.println(Message.INCORRECT_NUMBER);
                } else
                    System.out.println(Message.INCORRECT_NUMBER);
            }
        }
    }

    private static boolean isNumber(String strNum) {
        try {
            Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    public void updateTask() {
        System.out.println(Message.INSERT_PROJECT_NAME);
        Project project = selectProject();
        if (project != null) {
            int taskNumber = project.getTasks().size();
            if (taskNumber == 0) {
                System.out.println(Message.NO_TASKS_IN_PROJECT);
                return;
            }
            System.out.println(Message.IN_PROJECT + project.getName() + " "
                    + taskNumber + " " + Message.OF_TASKS_WITH_POINT + " " + Message.INSERT_TASK_NUMBER);
            boolean taskUpdated = false;
            while (!taskUpdated) {
                String number = scanner.nextLine();
                if (number.isEmpty()) break;
                if (isNumber(number)) {
                    int num = Integer.parseInt(number);
                    if (taskExists(num, taskNumber)) {
                        System.out.println(Message.INSERT_NEW_TASK);
                        String newTaskText = scanner.nextLine();
                        if (newTaskText.isEmpty()) break;
                        project.updateTask(num, new Task(project.getId(), newTaskText));
                        System.out.println(Message.TASK_UPDATED);
                        taskUpdated = true;
                    } else
                        System.out.println(Message.INCORRECT_NUMBER);
                } else System.out.println(Message.INCORRECT_NUMBER);
            }
        }
    }

    public void deleteTask() {
        System.out.println(Message.INSERT_PROJECT_NAME);
        Project project = selectProject();
        if (project != null) {
            int taskNumber = project.getTasks().size();
            System.out.println(Message.IN_PROJECT.toString() + taskNumber + " " + Message.OF_TASKS_WITH_POINT.toString() + " " + Message.INSERT_TASK_NUMBER);
            boolean taskDeleted = false;
            while (!taskDeleted) {
                String number = scanner.nextLine();
                if (number.isEmpty()) break;
                if (isNumber(number)) {
                    int num = Integer.parseInt(number);
                    if (taskExists(num, taskNumber)) {
                        project.deleteTask(num);
                        System.out.println(Message.TASK_DELETED);
                        taskDeleted = true;
                    }
                }
            }
        }
    }

    private static boolean taskExists(int input, int projectSize) {
        return input > 0 && input <= projectSize;
    }

    private static Project selectProject() {
        String projectName;
        while (true) {
            projectName = scanner.nextLine();
            if (projectName.isEmpty()) return null;
            for (Project project : projects) {
                if (projectName.equals(project.getName())) return project;
            }
            System.out.println(Message.PROJECT_NAME_DOESNT_EXIST + " " + Message.TRY_AGAIN);
        }
    }

}
