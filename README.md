TASK MANAGER

**Требования к software**\
JRE 1.8

**Cтек технологий**\
Java SE 1.8\
Maven

**Имя разработчика и контакты**\
Татьяна Дольникова
tatyanabulakhova@yandex.ru

**Сборка приложения**\
mvn clean install

**Запуск приложения**\
java -jar ./task-manager-1.0.0.jar